<?php

/**
 * Registers the `map_embed` post type.
 */
function map_embed_init() {
	register_post_type(
		'map-embed',
		[
			'labels'                => [
				'name'                  => __( 'Map embeds', 'nhs-map-embed' ),
				'singular_name'         => __( 'Map embed', 'nhs-map-embed' ),
				'all_items'             => __( 'All Map embeds', 'nhs-map-embed' ),
				'archives'              => __( 'Map embed Archives', 'nhs-map-embed' ),
				'attributes'            => __( 'Map embed Attributes', 'nhs-map-embed' ),
				'insert_into_item'      => __( 'Insert into map embed', 'nhs-map-embed' ),
				'uploaded_to_this_item' => __( 'Uploaded to this map embed', 'nhs-map-embed' ),
				'featured_image'        => _x( 'Featured Image', 'map-embed', 'nhs-map-embed' ),
				'set_featured_image'    => _x( 'Set featured image', 'map-embed', 'nhs-map-embed' ),
				'remove_featured_image' => _x( 'Remove featured image', 'map-embed', 'nhs-map-embed' ),
				'use_featured_image'    => _x( 'Use as featured image', 'map-embed', 'nhs-map-embed' ),
				'filter_items_list'     => __( 'Filter map embeds list', 'nhs-map-embed' ),
				'items_list_navigation' => __( 'Map embeds list navigation', 'nhs-map-embed' ),
				'items_list'            => __( 'Map embeds list', 'nhs-map-embed' ),
				'new_item'              => __( 'New Map embed', 'nhs-map-embed' ),
				'add_new'               => __( 'Add New', 'nhs-map-embed' ),
				'add_new_item'          => __( 'Add New Map embed', 'nhs-map-embed' ),
				'edit_item'             => __( 'Edit Map embed', 'nhs-map-embed' ),
				'view_item'             => __( 'View Map embed', 'nhs-map-embed' ),
				'view_items'            => __( 'View Map embeds', 'nhs-map-embed' ),
				'search_items'          => __( 'Search map embeds', 'nhs-map-embed' ),
				'not_found'             => __( 'No map embeds found', 'nhs-map-embed' ),
				'not_found_in_trash'    => __( 'No map embeds found in trash', 'nhs-map-embed' ),
				'parent_item_colon'     => __( 'Parent Map embed:', 'nhs-map-embed' ),
				'menu_name'             => __( 'Map embeds', 'nhs-map-embed' ),
			],
			'public'                => false,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title', 'editor' ],
			'has_archive'           => false,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => false,
			'rest_base'             => 'map-embed',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'map_embed_init' );

/**
 * Sets the post updated messages for the `map_embed` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `map_embed` post type.
 */
function map_embed_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['map-embed'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Map embed updated. <a target="_blank" href="%s">View map embed</a>', 'nhs-map-embed' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'nhs-map-embed' ),
		3  => __( 'Custom field deleted.', 'nhs-map-embed' ),
		4  => __( 'Map embed updated.', 'nhs-map-embed' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Map embed restored to revision from %s', 'nhs-map-embed' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Map embed published. <a href="%s">View map embed</a>', 'nhs-map-embed' ), esc_url( $permalink ) ),
		7  => __( 'Map embed saved.', 'nhs-map-embed' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Map embed submitted. <a target="_blank" href="%s">Preview map embed</a>', 'nhs-map-embed' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Map embed scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview map embed</a>', 'nhs-map-embed' ), date_i18n( __( 'M j, Y @ G:i', 'nhs-map-embed' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Map embed draft updated. <a target="_blank" href="%s">Preview map embed</a>', 'nhs-map-embed' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'map_embed_updated_messages' );

/**
 * Sets the bulk post updated messages for the `map_embed` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `map_embed` post type.
 */
function map_embed_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['map-embed'] = [
		/* translators: %s: Number of map embeds. */
		'updated'   => _n( '%s map embed updated.', '%s map embeds updated.', $bulk_counts['updated'], 'nhs-map-embed' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 map embed not updated, somebody is editing it.', 'nhs-map-embed' ) :
						/* translators: %s: Number of map embeds. */
						_n( '%s map embed not updated, somebody is editing it.', '%s map embeds not updated, somebody is editing them.', $bulk_counts['locked'], 'nhs-map-embed' ),
		/* translators: %s: Number of map embeds. */
		'deleted'   => _n( '%s map embed permanently deleted.', '%s map embeds permanently deleted.', $bulk_counts['deleted'], 'nhs-map-embed' ),
		/* translators: %s: Number of map embeds. */
		'trashed'   => _n( '%s map embed moved to the Trash.', '%s map embeds moved to the Trash.', $bulk_counts['trashed'], 'nhs-map-embed' ),
		/* translators: %s: Number of map embeds. */
		'untrashed' => _n( '%s map embed restored from the Trash.', '%s map embeds restored from the Trash.', $bulk_counts['untrashed'], 'nhs-map-embed' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'map_embed_bulk_updated_messages', 10, 2 );
