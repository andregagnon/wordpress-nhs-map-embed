<?php
/**
 * Block Name: Map Embed
 *
 * This is the template that displays the map embed.
 */

  $defaults = get_option( 'nhs_map_embed_plugin_options');

  // admin default option settings if field is unset(?)
  $id = get_field('id') ? get_field('id') : '111';
  $lat = get_field('lat') ? get_field('lat') : $defaults[ 'lat' ];
  $long = get_field('long') ? get_field('long') : $defaults[ 'long' ];
  $zoom = get_field('zoom') ?  get_field('zoom') : $defaults[ 'zoom' ];
  $width = get_field('width') ? get_field('width') : $defaults[ 'width' ];
  $height = get_field('height') ? get_field('height') : $defaults[ 'height' ];

// echo "<div> here $id, $lat, $long, $zoom</div>";
// echo "<pre>".print_r($attributes)."</pre>";

  ?>

  <div id="map-data-<?php echo $id; ?>" class="map-embed-data"
    data-id="<?php echo $id; ?>" data-lat="<?php echo $lat; ?>" data-long="<?php echo $long; ?>"
    data-zoom="<?php echo $zoom;?>" data-width="<?php echo $width;?>" data-height="<?php echo $height;?>"></div>
  <div id="map-target-<?php echo $id; ?>" style="width:<?php echo $width; ?>;height:<?php echo $height; ?>;" class="map-embed-target">Placeholder map-<?php echo $id; ?></div>
  <!-- <div id="map-target-<?php echo $id; ?>" class="map-embed-target">Placeholder map-<?php echo $id; ?></div> -->

  <?php
