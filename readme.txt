=== NHS Map embed ===
Contributors:
Donate link:
Tags: map
Requires at least: 4.5
Tested up to: 5.8.2
Requires PHP: 5.6
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Google map embeds with shortcode or block.

== Description ==

A general purpose google map embed for a WordPress site.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `nhs-map-embed.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==
