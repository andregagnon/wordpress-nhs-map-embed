<?php
/**
 * Plugin Name:     NHS Map embed
 * Plugin URI:      https://norfolkhistoricalsoiety.org
 * Description:     Embedde google map
 * Author:          Andre Gagnon
 * Author URI:      https://andregagnon.com
 * Text Domain:     nhs-map-embed
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Nhs_Map_Embed
 */

namespace NHSMapEmbed;

//*****************************************************************************
// admin screen for map embed settings

add_action( 'admin_menu', 'NHSMapEmbed\nhs_map_embed_submenu' );

// create submenu under settings
function nhs_map_embed_submenu() {
  add_options_page(
    'NHS Map Embed',
    'NHS Map Embed',
    'manage_options',
    'nhs_map_embed_submenu',
    'nhs_map_embed_options_page'
  );
}

// the options page html
function nhs_map_embed_options_page() {
  ?>
  <div class="wrap">
    <h2>NHS Map Embed</h2>
    <form action="options.php" method="post">
      <?php
      settings_fields( 'nhs_map_embed_plugin_options' );
      do_settings_sections( 'nhs_map_embed_submenu');
      submit_button( 'Save', 'primary' );
      ?>
    </form>
  </div>
  <?php
}

// register and define the options page settings
add_action( 'admin_init', 'NHSMapEmbed\nhs_map_embed_admin_init' );

function nhs_map_embed_admin_init() {

  $args = array(
    'type' => 'string',
    'sanitize_callback' => 'nhs_map_embed_validate_options',
    'default' => NULL,
  );

  register_setting( 'nhs_map_embed_plugin_options', 'nhs_map_embed_plugin_options', $args );

  add_settings_section(
    'nhs_map_embed_main',
    'NHS Map Embed Settings',
    'nhs_map_embed_section_text',
    'nhs_map_embed_submenu'
  );
  add_settings_field(
    'nhs_map_embed_api_key',
    'Google API Key',
    'nhs_map_embed_setting_api_key',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
  add_settings_field(
    'nhs_map_embed_zoom',
    'Map Zoom level',
    'nhs_map_embed_setting_zoom',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
  add_settings_field(
    'nhs_map_embed_lat',
    'Latitude',
    'nhs_map_embed_setting_lat',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
  add_settings_field(
    'nhs_map_embed_long',
    'Longitude',
    'nhs_map_embed_setting_long',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
  add_settings_field(
    'nhs_map_embed_width',
    'Width (px, %)',
    'nhs_map_embed_setting_width',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
  add_settings_field(
    'nhs_map_embed_height',
    'Height (px, %)',
    'nhs_map_embed_setting_height',
    'nhs_map_embed_submenu',
    'nhs_map_embed_main'
  );
}
function nhs_map_embed_section_text() {
  echo '<p>Enter settings.</p>';

}

function nhs_map_embed_setting_api_key() {

  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $api_key = $options[ 'api_key' ];

  // use option name[key] for name=
  echo "<input id='api_key' name='nhs_map_embed_plugin_options[api_key]'
    type='text' value='". esc_attr( $api_key) . "' size='50' />";

}

function nhs_map_embed_setting_zoom() {
  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $zoom = $options[ 'zoom' ];

  // use option name[key] for name=
  echo "<input id='zoom' name='nhs_map_embed_plugin_options[zoom]'
    type='text' value='". esc_attr( $zoom) . "' />";

}

function nhs_map_embed_setting_lat() {
  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $lat = $options[ 'lat' ];

  // use option name[key] for name=
  echo "<input id='lat' name='nhs_map_embed_plugin_options[lat]'
    type='text' value='". esc_attr( $lat) . "' />";

}
function nhs_map_embed_setting_long() {
  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $long = $options[ 'long' ];

  // use option name[key] for name=
  echo "<input id='long' name='nhs_map_embed_plugin_options[long]'
    type='text' value='". esc_attr( $long) . "' />";

}
function nhs_map_embed_setting_width() {
  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $width = $options[ 'width' ];

  // use option name[key] for name=
  echo "<input id='width' name='nhs_map_embed_plugin_options[width]'
    type='text' value='". esc_attr( $width) . "' />";

}
function nhs_map_embed_setting_height() {
  // get options by option name defined by register_settings()
  $options = get_option( 'nhs_map_embed_plugin_options' );
  $height = $options[ 'height' ];

  // use option name[key] for name=
  echo "<input id='height' name='nhs_map_embed_plugin_options[height]'
    type='text' value='". esc_attr( $height) . "' />";

}
function nhs_map_embed_validate_options( $input ) {

  $valid = array();

  $valid['api_key'] = sanitize_text_field( $input['api_key'] );
  $valid['zoom'] = sanitize_text_field( $input['zoom'] );
  $valid['lat'] = sanitize_text_field( $input['lat'] );
  $valid['long'] = sanitize_text_field( $input['long'] );
  $valid['width'] = sanitize_text_field( $input['width'] );
  $valid['height'] = sanitize_text_field( $input['height'] );

  return( $valid );
}

//*****************************************************************************
// shortcode versioun
// initialize shortcode

add_action( 'init', 'NHSMapEmbed\nhs_register_shortcode');
function nhs_register_shortcode() {

  // add shortcode
  add_shortcode( 'nhs_map_embed', 'NHSMapEmbed\nhs_map_embed_shortcode' );

  // add javascript function tbd: enqueue at runtime
  wp_register_script('nhs-map-embed', plugin_dir_url( __FILE__ ).'js/map.js', array(), date('h:m:s'), true );
  wp_register_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDZ98wY8dGFK7RLdglDgIKh24usbPDbiNk&callback=initMap', array(), '', true);

}

add_action( 'wp_enqueue_scripts', 'NHSMapEmbed\nhs_enqueue_scripts' );
function nhs_enqueue_scripts() {
    wp_enqueue_script( 'nhs-map-embed' );
    wp_enqueue_script( 'google-map' );
}



// the map embed shortcode
function nhs_map_embed_shortcode( $attr ) {


  $index = 0;
  $defaults = get_option( 'nhs_map_embed_plugin_options' );

  // sanitize attr
  $attr = shortcode_atts(
      [
        // defaults.  TBD: put in admin screen
        'index' => $index,
        'id' => 'map' . $index,
        'lat' => $defaults[ 'lat' ],
        'long' => $defaults[ 'long' ],
        'zoom' => $defaults[ 'zoom' ],
        'width' => $defaults[ 'width' ],
        'height' => $defaults[ 'height' ],
      ],
      $attr,
      'nhs_map_embed'
    );

  $html = '<div id="'.$attr['id'].'" style="width:'.$attr['width'].';height:'. $attr['height'].';">map1</div>';

 // add data to javascript
  wp_add_inline_script( 'nhs-map-embed', 'var map_data = ' . wp_json_encode( $attr ), 'before' );

  // wp_add_inline_script( 'nhs-map-embed', 'var gmap_data[' . $index .'] = ' . wp_json_encode( $attr ), 'before' );

  return( $html );
}


//*****************************************************************************
// custom block for map embed using ACF

// Register Custom Blocks
add_action('acf/init', 'NHSMapEmbed\nhs_map_embed_register_block');
function nhs_map_embed_register_block() {

    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a place block.
        acf_register_block_type(array(
            'name'				=> 'map-embed',
            'title'				=> __( 'Map Embed'),
            'description'		=> __( 'A custom map embed block.'),
            'render_template'   => dirname( __FILE__ ) . '/blocks/map-embed/block.php',
            'category'			=> 'common',
            'icon'				=> 'location',
            'keywords'			=> array( 'map' ),
            'enqueue_style' => plugin_dir_url( __FILE__ ) . '/blocks/map-embed/map-embed.css',
        ));
    }
}


add_action( 'init', 'NHSMapEmbed\nhs_register_block_script');
function nhs_register_block_script() {

  $options = get_option( 'nhs_map_embed_plugin_options' );
  $api_key = $options[ 'api_key' ];

  // add javascript for blocks tbd: enqueue at runtime
  wp_register_script('nhs-map-embed-blocks', plugin_dir_url( __FILE__ ).'js/map-blocks.js', array(), date('h:m:s'), true );
  wp_register_script( 'google-map-blocks', 'https://maps.googleapis.com/maps/api/js?key=' . $api_key . '&callback=initMapBlocks', array(), '', true);

}

add_action( 'wp_enqueue_scripts', 'NHSMapEmbed\nhs_enqueue_block_scripts' );
function nhs_enqueue_block_scripts() {
    wp_enqueue_script( 'nhs-map-embed-blocks' );
    wp_enqueue_script( 'google-map-blocks' );
}

add_action( 'admin_enqueue_scripts', 'NHSMapEmbed\nhs_admin_enqueue_block_scripts' );
function nhs_admin_enqueue_block_scripts() {
    $options = get_option( 'nhs_map_embed_plugin_options' );
    $api_key = $options[ 'api_key' ];

    // add javascript for blocks tbd: enqueue at runtime
    wp_register_script('nhs-map-embed-blocks', plugin_dir_url( __FILE__ ).'js/map-blocks.js', array(), date('h:m:s'), true );
    // wp_register_script( 'google-map-blocks', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBwIpVAaM6KSIBoYhwJDmirWzIX1a8dgjM&callback=initMapBlocks', array(), '', true);
    wp_register_script( 'google-map-blocks', 'https://maps.googleapis.com/maps/api/js?key=' . $api_key . '&callback=initMapBlocks', array(), '', true);

    wp_enqueue_script( 'nhs-map-embed-blocks' );
    wp_enqueue_script( 'google-map-blocks' );
}
