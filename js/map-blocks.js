
function initMapBlocks() {

  // get all maps on this page
  var map_data = document.getElementsByClassName("map-embed-data");
  var i;
  // get data, build maps
  for( i = 0; i < map_data.length; i++ ) {
    var el, id, lat, long, zoom, width, height;

    // get map data embed in html data attributes
    el = map_data[ i ];
    id = parseInt( el.getAttribute("data-id") );
    lat = parseFloat( el.getAttribute("data-lat") );
    long = parseFloat( el.getAttribute("data-long") );
    zoom = parseInt( el.getAttribute("data-zoom") );
    width = el.getAttribute("data-width");
    height = el.getAttribute("data-height");

    // get center
    var center = { lat: lat , lng: long };
    var map_el = document.getElementById( 'map-target-'+ id );

    // style size
    // var style = "width:" + width + ";height:" + height + ";";
    // map_el.setAttribute( "style", style );

    // embed map
    var map = new google.maps.Map( map_el, {
      zoom: zoom,
      center: center,
    });

    // add a marker
    var marker = new google.maps.Marker({
      position: center,
      map: map,
      title: "Map " + id.toString(),
    });

  }
}
